package com.cegesoft.ibctest;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.cegesoft.ibc.IBCAPI;

public class SettingActivity extends AppCompatActivity {

    private Button saveButton, backButton;
    private EditText baud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        this.baud = this.findViewById(R.id.baud_edit);
        this.baud.setText(Integer.toString(IBCAPI.get().getConfig().getBaud()));
        this.saveButton = this.findViewById(R.id.saveButton);
        this.backButton = this.findViewById(R.id.backButton);
        this.saveButton.setOnClickListener(event -> {
            String baudStr = this.baud.getText().toString();
            int baud;
            try {
                baud = Integer.parseInt(baudStr);
            } catch (Exception e) {
                return;
            }
            IBCAPI.get().getConfig().setBaud(baud);
            this.finish();
        });
        this.backButton.setOnClickListener(event -> {
            this.finish();
        });
    }
}