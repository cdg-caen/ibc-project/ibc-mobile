package com.cegesoft.ibctest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.cegesoft.ibc.IBCAPI;
import com.cegesoft.ibc.user.User;

public class MainActivity extends AppCompatActivity {

    private Button fullSimButton;
    private Button imageButton;
    private Button settingsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //new Handler(Looper.getMainLooper()).postDelayed(generator::release, 210);
        this.fullSimButton = this.findViewById(R.id.simulation_button);
        this.fullSimButton.setOnClickListener(listener -> this.startActivity(new Intent(this, CompleteActivity.class)));
        this.imageButton = this.findViewById(R.id.image_button);
        this.imageButton.setOnClickListener(listener -> this.startActivity(new Intent(this, ImageActivity.class)));
        if (!IBCAPI.get().isInit()) {
            IBCAPI.get().init(this);
            IBCAPI.get().getConfig().setIP("10.0.0.2");
            User.CURRENT_USER = new User("0", "0", "", "");
            IBCAPI.get().setUsbManager((UsbManager)getSystemService(Context.USB_SERVICE));
        }
        this.settingsButton = this.findViewById(R.id.settings_button);
        this.settingsButton.setOnClickListener(this::onSettingClick);
    }

    public void onSettingClick(View view) {
        final Intent intent = new Intent(this, SettingActivity.class);
        this.startActivityForResult(intent, 1);
    }


}
