package com.cegesoft.ibctest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.cegesoft.ibc.IBCAPI;
import com.cegesoft.ibc.event.EventHandler;
import com.cegesoft.ibc.event.Listener;
import com.cegesoft.ibc.event.type.DeviceDisconnectedEvent;
import com.cegesoft.ibc.event.type.DeviceRecoverEvent;
import com.cegesoft.ibc.event.type.PixelReceiveEvent;
import com.cegesoft.ibc.process.ImageProcess;
import com.cegesoft.ibc.process.ProcessHandler;
import com.cegesoft.ibc.process.image.Location;
import com.cegesoft.ibc.process.image.Pixel;
import com.cegesoft.ibc.service.ReadService;
import com.cegesoft.ibc.service.RecoveryService;

import java.util.HashMap;
import java.util.Map;

public class ImageActivity extends AppCompatActivity implements Listener {

    private ImageView stateView;
    private ImageView imageView;
    private ProgressBar progressBar;
    private Button quitButton;

    private Bitmap bitmap;
    private boolean bip;

    private int width;
    private int height;

    private int truthDimensions = 0;

    private final HashMap<Location, Pixel> pixels = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        this.stateView = this.findViewById(R.id.connectedIndicImage);
        this.progressBar = this.findViewById(R.id.stateProgressBarImage);
        this.imageView = this.findViewById(R.id.imageView);
        this.quitButton = this.findViewById(R.id.quitButtonImage);
        this.quitButton.setOnClickListener(event -> {
            ProcessHandler.stopProcess();
            this.startActivity(new Intent(this, MainActivity.class));
        });
        IBCAPI.get().getEventManager().registerListener(this);
        bindService(new Intent(this, RecoveryService.class), RecoveryService.recoveryConnection, BIND_AUTO_CREATE);
        bindService(new Intent(this, ReadService.class), ReadService.readConnection, BIND_AUTO_CREATE);
        ProcessHandler.startProcess(new ImageProcess());
        this.detect(IBCAPI.get().getUsbSerialPort() != null);
        this.reset();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        IBCAPI.get().getEventManager().unregisterListener(this);
        ProcessHandler.stopProcess();
    }

    @EventHandler
    public void onDeviceDetected(DeviceRecoverEvent event) {
        this.runOnUiThread(() -> {
            detect(true);
        });
    }

    @EventHandler
    public void onDeviceDisconnected(DeviceDisconnectedEvent event) {
        this.runOnUiThread(() -> {
            detect(false);
        });
    }

    private void detect(boolean bool) {
        if (this.stateView != null)
            this.stateView.setImageResource(bool ? R.drawable.ibc_check : R.drawable.ibc_cross);
    }

    private void reset() {
        this.height = 0;
        this.width = 0;
        this.pixels.clear();
        this.bitmap = null;
        this.updateImage(null);
    }

    @EventHandler
    public void onReceivePixel(PixelReceiveEvent e) {
        if (e.getPacket() != null) {
            if (height == 0 && width == 0) {
                height = (int) e.getPacket().getDimension2D().getHeight();
                width = (int) e.getPacket().getDimension2D().getWidth();
            }
            if (height == (int)e.getPacket().getDimension2D().getHeight() && width == (int)e.getPacket().getDimension2D().getWidth()) {
                truthDimensions ++;
                if (pixels.keySet().stream().noneMatch(location -> location.getX() == e.getPacket().getLocation().getX() && location.getY() == e.getPacket().getLocation().getY())) {
                    pixels.put(e.getPacket().getLocation(), e.getPacket().getPixel());
                }
                this.updateImage(e);
            } else if (truthDimensions < 3)
                reset();
        }
    }

    private void updateImage(PixelReceiveEvent event) {
        if (event == null || width == 0 || height == 0) {
            runOnUiThread(() -> {
                this.progressBar.setProgress(0);
                this.imageView.setImageBitmap(null);
            });
            return;
        }
        int deltaW = this.width >= this.height ? 0 : (this.height - this.width)/2;
        int deltaH = this.height >= this.width ? 0 : (this.width - this.height)/2;
        double pixelW = this.imageView.getWidth() / (double)(this.width + deltaW * 2);
        double pixelH = this.imageView.getHeight() / (double)(this.height + deltaH * 2);

        if (bitmap == null) {
            bitmap = Bitmap.createBitmap(this.imageView.getWidth(), this.imageView.getHeight(), Bitmap.Config.ARGB_8888);
            for (int y = 0; y < bitmap.getHeight(); y++) {
                for (int x = 0; x < bitmap.getWidth(); x++) {
                    bitmap.setPixel(x, y, Color.rgb(255, 255, 255));
                }
            }
        }

        Pixel pixel = event.getPacket().getPixel();
        for (int i = (int)(event.getPacket().getLocation().getX()*pixelW); i < (int)(event.getPacket().getLocation().getX()*pixelW) + pixelW; i++) {
            for (int j = (int)(event.getPacket().getLocation().getY()*pixelH); j < (int)(event.getPacket().getLocation().getY()*pixelH) + pixelH; j++) {
                bitmap.setPixel((int)(deltaW*pixelW + i), (int)(deltaH*pixelH + j), pixel.getColor());
            }
        }
        runOnUiThread(() -> {
            this.imageView.setImageBitmap(bitmap);
            this.progressBar.setMin(0);
            this.progressBar.setMax(width*height);
            this.progressBar.setProgress(pixels.size());
            if (!bip && this.pixels.size() == height*width) {
                bip = true;
                ToneGenerator generator = new ToneGenerator(AudioManager.STREAM_ALARM, 250);
                generator.startTone(ToneGenerator.TONE_CDMA_ALERT_NETWORK_LITE, 200);
                new Handler(Looper.getMainLooper()).postDelayed(generator::release, 210);
            }
        });
    }
}