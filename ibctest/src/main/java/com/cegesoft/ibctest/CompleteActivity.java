package com.cegesoft.ibctest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cegesoft.ibc.IBCAPI;
import com.cegesoft.ibc.event.EventHandler;
import com.cegesoft.ibc.event.Listener;
import com.cegesoft.ibc.event.type.DeviceDisconnectedEvent;
import com.cegesoft.ibc.event.type.DeviceRecoverEvent;
import com.cegesoft.ibc.event.type.SerialReadEvent;
import com.cegesoft.ibc.event.type.UpdateProgressBarEvent;
import com.cegesoft.ibc.event.type.WriteOnScreenEvent;
import com.cegesoft.ibc.process.MainProcess;
import com.cegesoft.ibc.process.ProcessHandler;
import com.cegesoft.ibc.service.ReadService;
import com.cegesoft.ibc.service.RecoveryService;

public class CompleteActivity extends AppCompatActivity implements Listener {

    private TextView logView;
    private Button quitButton;
    private ImageView stateView;
    private ProgressBar progressBar;
    private MainProcess process;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete);
        IBCAPI.get().setCurrentActivity(this);
        this.logView = this.findViewById(R.id.logView);
        this.stateView = this.findViewById(R.id.connectedIndic);
        this.progressBar = this.findViewById(R.id.stateProgressBar);
        this.quitButton = this.findViewById(R.id.quitButton);
        this.quitButton.setOnClickListener(listener -> {
            ProcessHandler.stopProcess();
            this.startActivity(new Intent(this, MainActivity.class));
        });
        IBCAPI.get().getEventManager().registerListener(this);
        bindService(new Intent(this, RecoveryService.class), RecoveryService.recoveryConnection, BIND_AUTO_CREATE);
        bindService(new Intent(this, ReadService.class), ReadService.readConnection, BIND_AUTO_CREATE);
        if (IBCAPI.get().getUsbSerialPort() != null)
            this.detect(true);
        ProcessHandler.startProcess(process = new MainProcess(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        IBCAPI.get().getEventManager().unregisterListener(this);
        ProcessHandler.stopProcess();
    }

    private void info(String test) {
        this.logView.append(test);

        this.logView.scrollTo(0, Math.max(this.logView.getLayout().getLineTop(this.logView.getLineCount()) - this.logView.getHeight(), 0));
    }

    @EventHandler
    public void onDeviceDetected(DeviceRecoverEvent event) {
        this.runOnUiThread(() -> {
            this.detect(true);
        });
    }

    @EventHandler
    public void onDeviceDisconnected(DeviceDisconnectedEvent event) {
        this.runOnUiThread(() -> {
            this.detect(false);
        });
    }


    private void detect(boolean bool) {
        if (this.stateView != null) {
            this.stateView.setImageResource(bool ? R.drawable.ibc_check : R.drawable.ibc_cross);
            this.info("\nDevice " + (bool ? "recovered" : "disconnected"));

        }
    }

    @EventHandler
    public void onRead(SerialReadEvent event) {
        this.runOnUiThread(() -> {
            this.info(event.getRead());
        });
    }

    @EventHandler
    public void onRead(WriteOnScreenEvent event) {
        this.runOnUiThread(() -> {
            this.info("\n" + event.getMessage());
        });
    }

    @EventHandler
    public void onUpdateBar(UpdateProgressBarEvent event) {
        this.runOnUiThread(() -> {
            this.progressBar.setMin(event.getMin());
            this.progressBar.setMax(event.getMax());
            this.progressBar.setProgress(event.getProgress());
        });
    }


}