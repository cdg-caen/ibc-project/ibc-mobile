package com.cegesoft.ibc;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.HexDump;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private final String TAG = MainActivity.class.getSimpleName();

    private UsbManager mUsbManager;

    private UsbSerialDriver serial = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);

        refreshDeviceList();
    }

    @SuppressLint("StaticFieldLeak")
    private void refreshDeviceList() {

        new AsyncTask<Void, Void, UsbSerialDriver>() {
            @Override
            protected UsbSerialDriver doInBackground(Void... params) {
                Log.d(TAG, "Refreshing device list ...");
                while (true) {
                    SystemClock.sleep(1000);

                    final List<UsbSerialDriver> drivers = UsbSerialProber.getDefaultProber().findAllDrivers(mUsbManager);

                    for (final UsbSerialDriver driver : drivers) {
                        if (HexDump.toHexString(driver.getDevice().getVendorId()).equals("067B") && HexDump.toHexString(driver.getDevice().getProductId()).equals("2303")){
                            return driver;
                        }
                    }
                }
            }

            @Override
            protected void onPostExecute(UsbSerialDriver result) {
                serial = result;
                Log.d(TAG, "Device found.");
            }

        }.execute((Void) null);
    }

    public void onSettingClick(View view) {
    }
}
