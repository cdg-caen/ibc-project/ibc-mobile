package com.cegesoft.ibc;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.IBinder;
import android.widget.TextView;

import com.felhr.usbserial.UsbSerialDevice;
import com.felhr.usbserial.UsbSerialInterface;

import java.util.HashMap;
import java.util.Map;

public class Application implements Runnable {

    private Thread thread;
    private final Activity main;

    private UsbSerialInterface.UsbReadCallback mCallback = new UsbSerialInterface.UsbReadCallback() {
        @Override
        public void onReceivedData(byte[] bytes) {
            ((TextView) main.findViewById(R.id.timer)).setText(new String(bytes));
        }
    };

    public Application(Activity main) {
        this.main = main;
        (this.thread = new Thread(this)).start();
    }

    @Override
    public void run() {
        while (!thread.isInterrupted()) {
            UsbManager usbManager = (UsbManager) main.getSystemService(Context.USB_SERVICE);
            UsbDevice device = null;
            UsbDeviceConnection connection = null;
            HashMap<String, UsbDevice> map = usbManager.getDeviceList();
            if (map == null || map.isEmpty()) {
                ((TextView) main.findViewById(R.id.timer)).setText("Liste vide :(");
                return;
            }

            for (Map.Entry<String, UsbDevice> entry : map.entrySet()) {
                device = entry.getValue();
                int deviceVID = device.getVendorId();
                int devicePID = device.getProductId();
                ((TextView) main.findViewById(R.id.timer)).setText("VID : " + deviceVID);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ((TextView) main.findViewById(R.id.timer)).setText("PID : " + devicePID);
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (deviceVID != 0x1d6b || (devicePID != 0x0001 || devicePID != 0x0002 || devicePID != 0x0003)) {
                    connection = usbManager.openDevice(device);
                    break;
                } else {
                    device = null;
                    connection = null;
                }
            }

            if (device == null || connection == null) {
                ((TextView) main.findViewById(R.id.timer)).setText("Device ro Connection == null");
                return;
            }

            UsbSerialDevice serial = UsbSerialDevice.createUsbSerialDevice(device, connection);

            if (serial != null) {
                if (serial.open()) {
                    serial.setBaudRate(600);
                    serial.setDataBits(UsbSerialInterface.DATA_BITS_8);
                    serial.setStopBits(UsbSerialInterface.STOP_BITS_1);
                    serial.setParity(UsbSerialInterface.PARITY_NONE);
                    serial.setFlowControl(UsbSerialInterface.FLOW_CONTROL_OFF);
                    serial.read(mCallback);
                } else {
                    ((TextView) main.findViewById(R.id.timer)).setText("Can't open :(");
                }
            }
        }
    }

    public void interract() {
        this.thread.interrupt();
    }
}
