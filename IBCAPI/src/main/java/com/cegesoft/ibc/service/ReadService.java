package com.cegesoft.ibc.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.os.VibrationEffect;
import android.util.Log;

import androidx.annotation.Nullable;

import com.cegesoft.ibc.IBCAPI;
import com.cegesoft.ibc.bluetooth.BluetoothManager;
import com.cegesoft.ibc.event.EventHandler;
import com.cegesoft.ibc.event.EventManager;
import com.cegesoft.ibc.event.type.MessageReadEvent;
import com.cegesoft.ibc.event.type.SerialReadEvent;
import com.cegesoft.ibc.event.type.WriteOnScreenEvent;
import com.cegesoft.ibc.exceptions.IncorrectPacketFormatException;
import com.cegesoft.ibc.packets.IBCServerPacket;
import com.cegesoft.ibc.utils.VibratorUtil;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.logging.Logger;

public class ReadService extends Service {

    public static ReadService readService;
    public static ServiceConnection readConnection = new ServiceConnection() {
        /* (non-Javadoc)
         * @see android.content.ServiceConnection#onServiceConnected(android.content.ComponentName,* android.os.Ibinder)
         */
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            readService = ((ReadService.LocalBinder) rawBinder).getService();
        }

        /* (non-Javadoc)
         * @see
         * android.content.ServiceConnection#onServiceDisconnected(android.content.ComponentName)
         */
        public void onServiceDisconnected(ComponentName className) {
            readService = null;
        }
    };

    private final LocalBinder binder = new LocalBinder();

    private boolean started;
    private Thread task;

    @Override
    public void onCreate() {
        super.onCreate();
        started = true;
        task = new ReadTask();
        task.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        started = false;
        task.interrupt();
    }

    @Nullable
    @Override
    public LocalBinder onBind(Intent intent) {
        return binder;
    }

    public class LocalBinder extends Binder {
        public ReadService getService() {
            return ReadService.this;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class ReadTask extends Thread {

        @Override
        public void run() {
            boolean isNull = false;
            String backMessage = "";
            while (started) {
                try {
                    while (IBCAPI.get().getUsbSerialPort() == null && started) {
                        SystemClock.sleep(500);
                        isNull = true;
                    }
                    if (isNull) {
                        isNull = false;
                        Log.d(IBCAPI.TAG, "Port found : " + IBCAPI.get().getUsbSerialPort());
                        this.purgeBuffers();
                    }
                    String message = this.readMessage(backMessage);
                    try {
                        if (!message.contains("#"))
                            throw new Exception("Message doesn't contain stop character");
                        IBCAPI.writeMessage("Has '#' :");
                        String[] split = message.split("#");
                        for (int i = 0; i < split.length - (message.endsWith("#") ? 0 : 1); i++) {
                            IBCAPI.get().getEventManager().callEvent(new MessageReadEvent(split[i] + "#"));
                        }
                        backMessage = message.endsWith("#") ? "" : split[split.length - 1];
                    } catch (Exception e) {
                        Log.d(IBCAPI.TAG, "ERROR : " + e.getMessage());
                        e.printStackTrace();
                        this.purgeBuffers();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    this.purgeBuffers();
                }
                IBCAPI.writeMessage("Thread");
            }
            Log.d(IBCAPI.TAG, "Stopped");
        }

        private String cleanTextContent(String text) {
            String str = text.replaceAll("[^\\x00-\\x7F]", "").replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", "").replaceAll("\\p{C}", "").trim();
            int length = str.length();
            while (length > 3 && str.substring(length - 2).equals(str.substring(length - 4, length - 2))) {
                str = str.substring(0, length - 2);
                length = str.length();
            }

            return str;
        }

        private String readMessage(String str) {
            StringBuilder builder = new StringBuilder(str);
            long ms = System.currentTimeMillis();
            while ((!builder.toString().contains("#")) && System.currentTimeMillis() - ms < 2000) {
                byte[] buffer = new byte[128];
                if (IBCAPI.get().getUsbSerialPort() == null)
                    return builder.toString();
                try {
                    IBCAPI.get().getUsbSerialPort().read(buffer, 1000);
                } catch (IOException ignored) {
                    return "";
                }
                String receive = cleanTextContent(new String(buffer, StandardCharsets.UTF_8).replaceAll("`.", ""));
                Log.d(IBCAPI.TAG, receive);
                if (receive != null && !receive.equals("")) {
                    IBCAPI.get().getEventManager().callEvent(new SerialReadEvent(receive));
                    builder.append(receive);
                }
            }
            Log.d(IBCAPI.TAG, builder.toString());
            return builder.toString();
        }

        private void purgeBuffers() {
            try {
                IBCAPI.get().getUsbSerialPort().purgeHwBuffers(false, true);
            } catch (IOException e) {
                Log.e(IBCAPI.TAG, "Can't purge buffers cache", e);
            }
        }
    }

}
