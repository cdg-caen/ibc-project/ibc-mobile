package com.cegesoft.ibc.service;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.hardware.usb.UsbDeviceConnection;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;

import androidx.annotation.Nullable;

import com.cegesoft.ibc.IBCAPI;
import com.cegesoft.ibc.event.EventManager;
import com.cegesoft.ibc.event.type.DeviceDisconnectedEvent;
import com.cegesoft.ibc.event.type.DeviceRecoverEvent;
import com.cegesoft.ibc.packets.IBCClientPacket;
import com.cegesoft.ibc.utils.SerialUtil;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.io.IOException;
import java.util.List;

public class RecoveryService extends Service {

    public static RecoveryService recoveryService;

    public static ServiceConnection recoveryConnection = new ServiceConnection() {
        /* (non-Javadoc)
         * @see android.content.ServiceConnection#onServiceConnected(android.content.ComponentName,* android.os.Ibinder)
         */
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            recoveryService = ((RecoveryService.LocalBinder) rawBinder).getService();
        }

        /* (non-Javadoc)
         * @see
         * android.content.ServiceConnection#onServiceDisconnected(android.content.ComponentName)
         */
        public void onServiceDisconnected(ComponentName className) {
            recoveryService = null;
        }
    };

    private final LocalBinder binder = new LocalBinder();

    private boolean started;
    private RecoveryTask task;

    @Override
    public void onCreate() {
        super.onCreate();
        started = true;
        Log.d(IBCAPI.TAG, "Start Recovery");
        task = new RecoveryTask();
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        started = false;
        Log.d(IBCAPI.TAG, "Stop Recovery");
        task.cancel(false);
    }

    @Nullable
    @Override
    public LocalBinder onBind(Intent intent) {
        return binder;
    }

    public class LocalBinder extends Binder {
        public RecoveryService getService() {
            return RecoveryService.this;
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class RecoveryTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(IBCAPI.TAG, "Refreshing device list ...");
            while (started) {
                boolean alreadyConnected = IBCAPI.get().getUsbSerialPort() != null;
                try {
                    SystemClock.sleep(500);
                    final List<UsbSerialDriver> drivers = UsbSerialProber.getDefaultProber().findAllDrivers(IBCAPI.get().getUsbManager());
                    boolean deviceFound = false;
                    for (final UsbSerialDriver driver : drivers) {
                        if (alreadyConnected && driver.getPorts().stream().map(UsbSerialPort::getPortNumber).anyMatch(port -> port == IBCAPI.get().getUsbSerialPort().getPortNumber())) {
                            deviceFound = true;
                            break;
                        }
                        if (!alreadyConnected && !driver.getPorts().isEmpty() && SerialUtil.isValidProduct(driver)) {
                            UsbDeviceConnection connection;
                            try {
                                connection = IBCAPI.get().getUsbManager().openDevice(driver.getDevice());
                                if (connection == null)
                                    throw new SecurityException();
                            } catch (SecurityException ignored) {
                                IBCAPI.get().getUsbManager().requestPermission(driver.getDevice(), PendingIntent.getActivity(RecoveryService.this, 404, new Intent(Intent.ACTION_ANSWER), 0));
                                SystemClock.sleep(3000);
                                break;
                            }
                            UsbSerialPort port = driver.getPorts().get(0);
                            port.open(connection);
                            port.setParameters(IBCAPI.get().getConfig().getBaud(), UsbSerialPort.DATABITS_8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);

                            IBCAPI.get().setUsbSerialPort(port);
                            IBCAPI.get().getEventManager().callEvent(new DeviceRecoverEvent(port));
                            Log.d(IBCAPI.TAG, "Device found.");

                            deviceFound = true;
                            break;
                        }
                    }
                    if (alreadyConnected && !deviceFound) {
                        IBCAPI.get().getEventManager().callEvent(new DeviceDisconnectedEvent());
                        IBCAPI.get().setUsbSerialPort(null);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

//        @Override
//        protected void onPostExecute(UsbSerialPort result) {
//            state.setBackgroundColor(Color.parseColor("#FF2E7D32"));
//            state.setText(R.string.state_ready);
//
//            device = result;
//        }
    }

}
