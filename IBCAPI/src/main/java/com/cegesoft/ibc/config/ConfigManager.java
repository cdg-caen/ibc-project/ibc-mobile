package com.cegesoft.ibc.config;

import android.content.Context;

import com.cegesoft.ibc.user.User;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ConfigManager {
    private JSONObject json;
    private Context context;
    private final String fileName = "client.json";

    private int baud;
    private String ipAddress;
    private int port;
    private int limit;

    public ConfigManager(Context context) {
        try {
            this.context = context;
            if (!new File(context.getFilesDir().getAbsolutePath() + "/" + fileName).exists()){
                JSONObject object = new JSONObject();
                object.put("socket_ip", "10.0.0.5");
                object.put("socket_port", 35000);
                object.put("baud", 9600);
                json = object;
                saveConfig();
            }
            json = (JSONObject) new JSONParser().parse(new InputStreamReader(context.openFileInput(fileName)));
            this.load();
        } catch (Exception ignored) {
        }
    }

    public void reset() {
        this.json = new JSONObject();
        this.saveConfig();
    }

    private void saveConfig() {
        try {
            OutputStreamWriter writer = new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE));
            writer.write(json.toString());
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public User getUser() {
        JSONObject obj = (JSONObject) json.get("user");
        if (obj == null) {
            return null;
        }
        String id = (String) obj.get("id");
        String userId = (String) obj.get("userId");
        String sendKey = (String) obj.get("publicKey");
        String receiveKey = (String) obj.get("privateKey");
        return new User(id, userId, sendKey, receiveKey);
    }

    public void saveUser() {
        User user = User.CURRENT_USER;
        if (user == null) {
            json.remove("user");
            saveConfig();
            return;
        }
        JSONObject userJ = new JSONObject();
        userJ.put("publicKey", user.getSendKey());
        userJ.put("privateKey", user.getReceiveKey());
        userJ.put("id", user.getId());
        userJ.put("userId", user.getUserId());
        json.remove("user");
        json.put("user", userJ);
        saveConfig();
    }

    private void load() {
        this.baud = this.getBaudFromConfig();
        this.ipAddress = (String) json.get("socket_ip");
        this.port = this.getPortFromConfig();
        this.limit = Integer.parseInt(json.getOrDefault("message_limit", "2") + "");
    }

    public String getIP(){
        return this.ipAddress;
    }

    public void setIP(String ip){
        this.ipAddress = ip;
        json.replace("socket_ip", ip);
        saveConfig();
    }

    public void setPort(int port){
        this.port = port;
        json.replace("socket_port", port);
        saveConfig();
    }

    public int getPort() {
        return this.port;
    }

    public int getBaud() {
        return this.baud;
    }

    private int getPortFromConfig(){
        int i;
        if (json.get("socket_port") instanceof Long)
            i = (int) (long) json.get("socket_port");
        else
            i = (int) json.get("socket_port");
        return i;
    }

    private int getBaudFromConfig() {
        int i;
        if (json.getOrDefault("baud", 9600) instanceof Long)
            i = (int) (long) json.getOrDefault("baud", 9600L);
        else
            i = (int) json.getOrDefault("baud", 9600L);
        return i;
    }

    public void setBaud(int baud) {
        this.baud = baud;
        json.replace("baud", baud);
        saveConfig();
    }

    public int getMessageLimit() {
        return this.limit;
    }

    public void setMessageLimit(int messageLimit) {
        this.limit = messageLimit;
        json.remove("message_limit");
        json.put("message_limit", Integer.toString(messageLimit));
        saveConfig();
    }
}
