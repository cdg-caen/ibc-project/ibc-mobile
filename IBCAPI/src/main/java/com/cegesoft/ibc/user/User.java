package com.cegesoft.ibc.user;

public class User {

    public static User CURRENT_USER;
    private final String id;
    private final String userId;
    private String sendKey;
    private String receiveKey;


    public User(String id, String userId, String sendKey, String receiveKey) {
        this.id = id;
        this.userId = userId;
        this.sendKey = sendKey;
        this.receiveKey = receiveKey;
    }

    public String getId() {
        return id;
    }

    public String getSendKey() {
        return sendKey;
    }

    public void setSendKey(String sendKey) {
        this.sendKey = sendKey;
    }

    public String getUserId() {
        return userId;
    }

    public String declare(String name, String serverReceiveKey, String serverSendKey){
        return userId + "#" + name + "#CLIENT#" + serverReceiveKey.replace("/", "%") + "#" + serverSendKey.replace("/", "%") + "#" + id;
    }

    public String getReceiveKey() {
        return receiveKey;
    }
}
