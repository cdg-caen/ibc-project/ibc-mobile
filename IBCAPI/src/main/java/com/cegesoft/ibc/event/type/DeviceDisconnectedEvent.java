package com.cegesoft.ibc.event.type;

import com.cegesoft.ibc.event.Event;
import com.cegesoft.ibc.event.HandlerList;

public class DeviceDisconnectedEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public DeviceDisconnectedEvent() {
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
