package com.cegesoft.ibc.event.type;

import com.cegesoft.ibc.event.Event;
import com.cegesoft.ibc.event.HandlerList;

public class WriteOnScreenEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final String message;

    public WriteOnScreenEvent(String message) {
        this.message = message;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public String getMessage() {
        return message;
    }
}