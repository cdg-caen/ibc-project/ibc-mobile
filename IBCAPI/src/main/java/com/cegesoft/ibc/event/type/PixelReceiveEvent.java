package com.cegesoft.ibc.event.type;

import com.cegesoft.ibc.event.Event;
import com.cegesoft.ibc.event.HandlerList;
import com.cegesoft.ibc.process.image.PixelPacket;

public class PixelReceiveEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final PixelPacket packet;

    public PixelReceiveEvent(PixelPacket packet) {
        this.packet = packet;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public PixelPacket getPacket() {
        return packet;
    }
}