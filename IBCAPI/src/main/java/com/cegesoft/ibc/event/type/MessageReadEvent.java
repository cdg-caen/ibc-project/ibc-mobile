package com.cegesoft.ibc.event.type;

import com.cegesoft.ibc.event.Event;
import com.cegesoft.ibc.event.HandlerList;

public class MessageReadEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final String message;

    public MessageReadEvent(String message) {
        this.message = message;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public String getMessage() {
        return message;
    }
}
