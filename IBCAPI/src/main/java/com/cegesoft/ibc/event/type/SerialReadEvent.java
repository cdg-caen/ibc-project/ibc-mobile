package com.cegesoft.ibc.event.type;

import com.cegesoft.ibc.event.Event;
import com.cegesoft.ibc.event.HandlerList;

public class SerialReadEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final String read;

    public SerialReadEvent(String read) {
        this.read = read;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public String getRead() {
        return read;
    }
}
