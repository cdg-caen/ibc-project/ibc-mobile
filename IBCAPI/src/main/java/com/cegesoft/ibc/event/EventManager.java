package com.cegesoft.ibc.event;

import com.cegesoft.ibc.utils.Tuple;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * Created by HoxiSword on 06/04/2020 for BBCollabBot
 */
public class EventManager {

    public void registerListener(Listener listener) {
        Class<? extends Listener> listenerClass = listener.getClass();
        for (Method method : listenerClass.getMethods()) {
            EventHandler[] annotations = method.getAnnotationsByType(EventHandler.class);
            if (annotations.length == 0)
                continue;
            if (method.getParameterCount() != 1)
                continue;
            if (!Event.class.isAssignableFrom(method.getParameterTypes()[0]))
                continue;
            Class<? extends Event> eventClass = (Class<? extends Event>)method.getParameterTypes()[0];
            try {
                Method handlerListGetter = eventClass.getMethod("getHandlerList");
                HandlerList list = (HandlerList)handlerListGetter.invoke(null);
                list.addListener(new Tuple<>(listener, method));
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ignored) {
            }
        }
    }

    public void unregisterListener(Listener listener) {
        Class<? extends Listener> listenerClass = listener.getClass();
        for (Method method : listenerClass.getMethods()) {
            EventHandler[] annotations = method.getAnnotationsByType(EventHandler.class);
            if (annotations.length == 0)
                continue;
            if (method.getParameterCount() != 1)
                continue;
            if (!Event.class.isAssignableFrom(method.getParameterTypes()[0]))
                continue;
            Class<? extends Event> eventClass = (Class<? extends Event>)method.getParameterTypes()[0];
            try {
                Method handlerListGetter = eventClass.getMethod("getHandlerList");
                HandlerList list = (HandlerList)handlerListGetter.invoke(null);
                list.removeListener(new Tuple<>(listener, method));
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ignored) {
            }
        }
    }

    public void callEvent(Event event) {
        try {
            HandlerList handlerList = (HandlerList)event.getClass().getDeclaredMethod("getHandlerList").invoke(null);
            handlerList.callListeners(event);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException ignored) {
        }
    }
}
