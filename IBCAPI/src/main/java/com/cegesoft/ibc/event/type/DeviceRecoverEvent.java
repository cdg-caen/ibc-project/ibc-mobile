package com.cegesoft.ibc.event.type;

import com.cegesoft.ibc.event.Event;
import com.cegesoft.ibc.event.HandlerList;
import com.hoho.android.usbserial.driver.UsbSerialPort;

public class DeviceRecoverEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    public static HandlerList getHandlerList() {
        return handlers;
    }

    private final UsbSerialPort port;

    public DeviceRecoverEvent(UsbSerialPort port) {
        this.port = port;
    }

    public UsbSerialPort getPort() {
        return port;
    }
}
