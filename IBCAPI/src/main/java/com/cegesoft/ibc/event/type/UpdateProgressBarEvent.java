package com.cegesoft.ibc.event.type;

import com.cegesoft.ibc.event.Event;
import com.cegesoft.ibc.event.HandlerList;

public class UpdateProgressBarEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    private final int min, max, progress;

    public UpdateProgressBarEvent(int min, int max, int progress) {
        this.min = min;
        this.max = max;
        this.progress = progress;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

    public int getProgress() {
        return progress;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
