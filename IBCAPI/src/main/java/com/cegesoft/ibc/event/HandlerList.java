package com.cegesoft.ibc.event;

import android.util.Log;

import com.cegesoft.ibc.IBCAPI;
import com.cegesoft.ibc.utils.Tuple;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HoxiSword on 06/04/2020 for BBCollabBot
 */
public class HandlerList {

    private final List<Tuple<Listener, Method>> listeners = new ArrayList<>();

    public HandlerList() {

    }

    void addListener(Tuple<Listener, Method> method) {
        listeners.add(method);
    }

    void removeListener(Tuple<Listener, Method> method) {
        listeners.remove(method);
    }

    void callListeners(Object event) {
        for (Tuple<Listener, Method> method : listeners) {
            try {
                method.getB().invoke(method.getA(), event);
            } catch (Exception e) {
                Log.e(IBCAPI.TAG, "Can't call event : ", e);
            }
        }
    }

}
