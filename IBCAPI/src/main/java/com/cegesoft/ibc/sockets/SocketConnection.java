package com.cegesoft.ibc.sockets;

import android.util.Log;

import com.cegesoft.ibc.IBCAPI;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;


public class SocketConnection implements Closeable {

    private final Socket socket;
    private DataInputStream reader;
    private DataOutputStream writer;

    public SocketConnection(){
        this.socket = this.connect();
    }

    private Socket connect() {
        Socket socket = new Socket();
        Log.d("", " --- Connection to " + IBCAPI.get().getConfig().getIP());
        try {
            socket.connect(new InetSocketAddress(IBCAPI.get().getConfig().getIP(), IBCAPI.get().getConfig().getPort()));
            reader = new DataInputStream(socket.getInputStream());
            writer = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        Log.d("", "Connected.");
        return socket;
    }

    public void sendMessage(String message) throws IOException {
        if (writer == null)
            return;
        writer.writeUTF(message);
        Log.d("SEND", "Successfully sent : " + message);
    }

    public void sendMessage(Message message, boolean encrypt) throws Exception {
        this.sendMessage(message.parse(encrypt));
    }

    @Override
    public void close() throws IOException {
        writer.close();
        reader.close();
        socket.close();
    }

    public String waitForAnswer() throws IOException {
        if (reader == null)
            return "";
        return reader.readUTF();
    }


}

