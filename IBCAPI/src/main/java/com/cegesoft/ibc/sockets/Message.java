package com.cegesoft.ibc.sockets;

import com.cegesoft.ibc.crypto.CryptoHandler;
import com.cegesoft.ibc.exceptions.MessageBadlyFormattedException;
import com.cegesoft.ibc.user.User;

public class Message {
    private final Type type;
    private User sender;
    private final Request request;
    private final String[] args;

    public Message(String message) throws MessageBadlyFormattedException {
        if (!message.contains(":") || message.split(":").length < 3){
            throw new MessageBadlyFormattedException(message);
        }
        String[] split = message.split(":");

        try {
            type = Type.valueOf(split[0].toUpperCase());
        } catch (Exception ex){
            throw new MessageBadlyFormattedException(message, ex);
        }

        try {
            request = Request.valueOf(split[1].toUpperCase());
        } catch (Exception ex){
            throw new MessageBadlyFormattedException(message, ex);
        }

        sender = new User("", split[2], "", "");

        args = new String[split.length-3];

        System.arraycopy(split, 3, args, 0, split.length - 3);
    }

    public Message(Type type, Request request, User id, String... args){
        this.type = type;
        this.request = request;
        this.sender = id;
        this.args = args;
    }

    @Override
    public String toString() {
        return "Message[type=" + type.name() + ", request=" + request.name() + "]";
    }

    public String parse(boolean encrypt) throws Exception {
        StringBuilder args = new StringBuilder();
        if (this.args.length >= 1)
            args.append(this.args[0]);
        if (this.args.length >= 2)
            for (int i = 1; i < this.args.length; i++)
                args.append(":").append(this.args[i]);
        String msg = type.name() + ":" + request.name() + ":" + sender.getUserId() + ":" + args.toString();
        if (!encrypt)
            return msg;
        return CryptoHandler.encrypt(msg, sender.getSendKey()) + ";" + sender.getId();
    }

    public Type getType() {
        return type;
    }

    public User getSender() {
        return sender;
    }

    public String[] getArgs() {
        return args;
    }

    public Request getRequest() {
        return request;
    }

    public enum Type {
        GET,
        POST;

        @Override
        public String toString() {
            return name();
        }
    }

    public enum Request {
        MONEY,
        TICKETS,
        CODE,
        DECLARE,
        NAME,
        QUERY;

        @Override
        public String toString() {
            return name();
        }
    }

}
