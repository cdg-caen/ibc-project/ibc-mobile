package com.cegesoft.ibc.crypto;

import android.util.Base64;

import javax.crypto.SecretKey;

public class CryptoHandler {

    public static String encrypt(String data, String publicKey) {
        try {
            SecretKey AESKey = AES.generateKey();
            String AESKeyString = Base64.encodeToString(AESKey.getEncoded(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
            String crypted = Base64.encodeToString(AES.encrypt(data, AESKey), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE);
            return crypted + "!" + RSA.encrypt(AESKeyString, publicKey);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String decrypt(String message, String privateKey) {
        try {
            String crypted = message.split("!")[0];
            String cryptedKey = message.split("!")[1];

            String key = RSA.decrypt(cryptedKey, privateKey);
            return new String(AES.decrypt(Base64.decode(crypted.getBytes(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE), key));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

}
