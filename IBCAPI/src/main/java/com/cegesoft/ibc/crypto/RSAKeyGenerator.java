package com.cegesoft.ibc.crypto;

import android.util.Base64;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

public class RSAKeyGenerator {
    private PrivateKey privateKey;
    private PublicKey publicKey;

    public RSAKeyGenerator() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(512);
        KeyPair pair = keyGen.generateKeyPair();
        this.privateKey = pair.getPrivate();
        this.publicKey = pair.getPublic();
    }

    private PrivateKey getPrivateKey() {
        return privateKey;
    }

    private PublicKey getPublicKey() {
        return publicKey;
    }

    public String getPrivateKeyString() {
        return Base64.encodeToString(this.getPrivateKey().getEncoded(), Base64.NO_WRAP | Base64.NO_PADDING);
    }

    public String getPublicKeyString() {
        return Base64.encodeToString(this.getPublicKey().getEncoded(), Base64.NO_WRAP | Base64.NO_PADDING);
    }
}
