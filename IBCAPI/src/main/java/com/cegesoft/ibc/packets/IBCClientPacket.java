package com.cegesoft.ibc.packets;

import com.cegesoft.ibc.user.User;

public class IBCClientPacket {

    private final IBCPacketType type;
    private final User user;
    private final String token;
    private final int amount;

    public IBCClientPacket(IBCPacketType type, User user, String token, int amount) {
        this.type = type;
        this.user = user;
        this.token = token;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return type.name() + ":" + user.getUserId() + ":" + token + ":" + amount + "#";
    }
}

