package com.cegesoft.ibc.packets;

import androidx.annotation.NonNull;

import com.cegesoft.ibc.exceptions.IncorrectPacketFormatException;

public class IBCServerPacket {

    private final IBCPacketType type;
    private final String token;
    private final String address;
    private final int amount;

    public IBCServerPacket(@NonNull String packet) throws IncorrectPacketFormatException {
        if (!packet.contains(":")) {
            throw new IncorrectPacketFormatException(packet);
        }

        String[] split = packet.replace("#", "").split(":");

        if (split.length != 4) {
            throw new IncorrectPacketFormatException(packet);
        }

        String typeName = split[0];
        this.token = split[1];
        this.address = split[2];
        try {
            this.amount = Integer.parseInt(split[3]);
            this.type = IBCPacketType.valueOf(typeName);
        } catch (Exception e) {
            throw new IncorrectPacketFormatException(packet);
        }
    }

    public IBCPacketType getType() {
        return type;
    }

    public String getToken() {
        return token;
    }

    public String getAddress() {
        return address;
    }

    public int getAmount() {
        return amount;
    }
}

