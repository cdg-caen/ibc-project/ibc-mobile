package com.cegesoft.ibc.packets;

public enum IBCPacketType {

    M,
    D,
    U

}