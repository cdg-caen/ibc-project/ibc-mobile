package com.cegesoft.ibc.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHidDeviceAppQosSettings;
import android.util.Log;

import com.cegesoft.ibc.IBCAPI;
import com.cegesoft.ibc.crypto.CryptoHandler;
import com.cegesoft.ibc.packets.IBCClientPacket;
import com.cegesoft.ibc.packets.IBCServerPacket;
import com.cegesoft.ibc.user.User;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static java.nio.charset.StandardCharsets.UTF_8;

public class BluetoothManager {

    private BluetoothConnector.BluetoothSocketWrapper wrapper;

    private BluetoothDevice target;

    private String parseAddress(IBCServerPacket packet) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < packet.getAddress().length(); i++) {
            builder.append(packet.getAddress().charAt(i));
            if (i % 2 != 0 && i != packet.getAddress().length() - 1) {
                builder.append(":");
            }
        }
        return builder.toString();
    }

    private void initBluetoothAddress(String address) {
        this.target = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(address);
    }

    private BluetoothConnector getConnector() {
        return new BluetoothConnector(this.target, true, BluetoothAdapter.getDefaultAdapter(), null);
    }

    public void connect(IBCServerPacket targetPacket) throws IOException {
        String address = this.parseAddress(targetPacket);
        Log.d(IBCAPI.TAG, "Target address : " + address);
        this.initBluetoothAddress(address);

        if (this.target == null) {
            Log.e(IBCAPI.TAG, "Target = null");
            return;
        }

        this.wrapper = this.getConnector().connect();
    }

    public boolean isConnected() {
        return this.wrapper != null && this.wrapper.getUnderlyingSocket().isConnected();
    }

    public boolean send(String message) throws IOException {
        if (isConnected()) {
            this.wrapper.getOutputStream().write(message.getBytes(UTF_8));
            return true;
        }
        return false;
    }

    public String encryptMessage(IBCServerPacket packet) {
        return (CryptoHandler.encrypt(new IBCClientPacket(packet.getType(), User.CURRENT_USER, IBCAPI.get().isWrongToken() ? "wrong" : packet.getToken(), packet.getAmount()).toString(), User.CURRENT_USER.getSendKey()) + ";" + User.CURRENT_USER.getId() + ";" + (IBCAPI.get().isWrongToken() ? "wrong" : packet.getToken()));
    }

    public void close() {
        try {
            this.wrapper.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
