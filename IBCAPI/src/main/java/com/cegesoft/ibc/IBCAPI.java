package com.cegesoft.ibc;

import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbManager;

import com.cegesoft.ibc.bluetooth.BluetoothManager;
import com.cegesoft.ibc.config.ConfigManager;
import com.cegesoft.ibc.event.EventManager;
import com.cegesoft.ibc.event.type.WriteOnScreenEvent;
import com.hoho.android.usbserial.driver.UsbSerialPort;

public class IBCAPI {

    public static final String TAG = "[IBC]";
    private static IBCAPI api;

    private EventManager eventManager;
    private boolean init;
    private ConfigManager config;
    private BluetoothManager bluetoothManager;
    private UsbSerialPort usbSerialPort;
    private UsbManager usbManager;
    private boolean wrongToken;

    public Activity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(Activity currentActivity) {
        this.currentActivity = currentActivity;
    }

    private Activity currentActivity;

    private IBCAPI() {
    }

    public static IBCAPI get() {
        if (api == null)
            api = new IBCAPI();
        return api;
    }

    public BluetoothManager getBluetoothManager() {
        return bluetoothManager;
    }

    public UsbSerialPort getUsbSerialPort() {
        return usbSerialPort;
    }

    public void setUsbSerialPort(UsbSerialPort usbSerialPort) {
        this.usbSerialPort = usbSerialPort;
    }

    public void init(Context context) {
        if (this.init)
            return;
        this.init = true;
        this.config = new ConfigManager(context);
        this.bluetoothManager = new BluetoothManager();
        this.eventManager = new EventManager();
    }

    public ConfigManager getConfig() {
        return this.config;
    }

    public boolean isInit() {
        return this.init;
    }

    public UsbManager getUsbManager() {
        return usbManager;
    }

    public void setUsbManager(UsbManager usbManager) {
        this.usbManager = usbManager;
    }

    public EventManager getEventManager() {
        return eventManager;
    }

    public static void writeMessage(String s) {
        IBCAPI.get().getEventManager().callEvent(new WriteOnScreenEvent(s));
    }

    public boolean isWrongToken() {
        return wrongToken;
    }

    public void setWrongToken(boolean wrongToken) {
        this.wrongToken = wrongToken;
    }
}
