package com.cegesoft.ibc.utils;

import com.hoho.android.usbserial.driver.UsbSerialDriver;

import java.util.ArrayList;
import java.util.List;

public class SerialUtil {

    private static final List<Tuple<Integer, Integer>> products;

    static {
        products = new ArrayList<>();
        products.add(new Tuple<>(1027, 24577));
        products.add(new Tuple<>(1659, 8963));
    }

    public static boolean isValidProduct(final UsbSerialDriver driver) {
        return products.stream().anyMatch(tuple -> tuple.getA() == driver.getDevice().getVendorId() && tuple.getB() == driver.getDevice().getProductId());
    }


}
