package com.cegesoft.ibc.utils;


import android.util.Base64;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import static java.nio.charset.StandardCharsets.UTF_8;

public class AES {

    private static final List<Character> blacklist = Arrays.asList(';', ':');


    public static byte[] encrypt(String message, String key) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(Base64.decode(key.getBytes(UTF_8), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE), "AES"), new IvParameterSpec("AAAAAAAAAAAAAAAA".getBytes(UTF_8)));
        return cipher.doFinal(message.getBytes(UTF_8));
    }

    public static SecretKey generateKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(256);
        SecretKey key = keyGen.generateKey();
        for (char c : blacklist)
            if (new String(Base64.encode(key.getEncoded(), Base64.NO_WRAP | Base64.NO_PADDING | Base64.URL_SAFE), UTF_8).contains(c + ""))
                return generateKey();
        return key;
    }
}
