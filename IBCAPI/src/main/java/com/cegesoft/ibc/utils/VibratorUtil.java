package com.cegesoft.ibc.utils;

import android.content.Context;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;

import com.cegesoft.ibc.IBCAPI;

public class VibratorUtil {

    public static void vibrate(Context context, VibrationEffect effect) {
        Vibrator vibrator = ((Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE));
        if (vibrator == null) {
            Log.e(IBCAPI.TAG, "Vibrator = null");
            return;
        }
        vibrator.vibrate(effect);
    }

}
