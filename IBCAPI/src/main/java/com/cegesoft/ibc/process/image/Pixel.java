package com.cegesoft.ibc.process.image;

import com.cegesoft.ibc.exceptions.IncorrectPacketFormatException;

public class Pixel {

    private final int color;
    private final int red;
    private final int green;
    private final int blue;
    private final int alpha;

    Pixel(int pixel) {
        this.color = pixel;
        this.alpha = (pixel >> 24) & 0xff;
        this.red = (pixel >> 16) & 0xff;
        this.green = (pixel >> 8) & 0xff;
        this.blue = (pixel) & 0xff;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }

    public int getAlpha() {
        return alpha;
    }

    public static Pixel fromString(String s) throws IncorrectPacketFormatException {
        return new Pixel(-Integer.parseInt(s));
    }

    public int getColor() {
        return color;
    }
}