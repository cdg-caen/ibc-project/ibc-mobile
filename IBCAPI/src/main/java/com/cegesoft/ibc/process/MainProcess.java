package com.cegesoft.ibc.process;

import android.content.Context;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.SystemClock;
import android.os.VibrationEffect;
import android.util.Log;

import com.cegesoft.ibc.IBCAPI;
import com.cegesoft.ibc.bluetooth.BluetoothManager;
import com.cegesoft.ibc.event.EventHandler;
import com.cegesoft.ibc.event.Listener;
import com.cegesoft.ibc.event.type.MessageReadEvent;
import com.cegesoft.ibc.event.type.UpdateProgressBarEvent;
import com.cegesoft.ibc.exceptions.IncorrectPacketFormatException;
import com.cegesoft.ibc.packets.IBCServerPacket;
import com.cegesoft.ibc.utils.VibratorUtil;

import java.io.IOException;

public class MainProcess extends Process implements Listener {

    private final Context currentContext;
    private int count = -1;
    private String message;
    private boolean queried;
    private boolean enabled;

    public MainProcess(Context currentContext) {
        this.currentContext = currentContext;
    }

    @Override
    protected void execute() {
        this.setEnabled(true);
        try {
            while (true) {
                Thread.sleep(500 * IBCAPI.get().getConfig().getMessageLimit());
                count = 0;
                if (queried)
                    continue;
                message = "";
                IBCAPI.get().getEventManager().callEvent(new UpdateProgressBarEvent(0, IBCAPI.get().getConfig().getMessageLimit(), count));
            }
        } catch (InterruptedException ignored) {

        }
    }

    @Override
    protected void interruptProcess() {
        this.setEnabled(false);
    }

    @EventHandler
    public void onMessage(MessageReadEvent event) {
        if (count == 0) {
            message = event.getMessage();
            IBCAPI.writeMessage("Count = 0 (1)");
        }
        if (count > 0) {
            if (!message.equals(event.getMessage())) {
                IBCAPI.writeMessage("Message différent (2)");
                count = 1;
                message = event.getMessage();
                IBCAPI.get().getEventManager().callEvent(new UpdateProgressBarEvent(0, IBCAPI.get().getConfig().getMessageLimit(), count));
                return;
            }
        }
        count++;
        IBCAPI.writeMessage(String.format("Count = %s (3)", count));
        IBCAPI.get().getEventManager().callEvent(new UpdateProgressBarEvent(0, IBCAPI.get().getConfig().getMessageLimit(), count));
        if (IBCAPI.get().getConfig().getMessageLimit() <= count) {
            IBCAPI.writeMessage("Query (4)");
            query();
        }
    }

    private void query() {
        queried = true;
        try {
            IBCServerPacket packet = new IBCServerPacket(message);
            Log.d(IBCAPI.TAG, "Message received : " + message);
            Log.d(IBCAPI.TAG, "Split Message : ");
            Log.d(IBCAPI.TAG, " => TYPE : " + packet.getType().name());
            Log.d(IBCAPI.TAG, " => BT ADDR : " + packet.getAddress());
            Log.d(IBCAPI.TAG, " => TOKEN : " + packet.getToken());
            Log.d(IBCAPI.TAG, " => AMOUNT : " + packet.getAmount());
            Log.d(IBCAPI.TAG, " => WRONG TOKEN : " + IBCAPI.get().isWrongToken());

            IBCAPI.writeMessage("Message received : " + message);
            IBCAPI.writeMessage("Split Message : ");
            IBCAPI.writeMessage(" => TYPE : " + packet.getType().name());
            IBCAPI.writeMessage(" => BT ADDR : " + packet.getAddress());
            IBCAPI.writeMessage(" => TOKEN : " + packet.getToken());
            IBCAPI.writeMessage(" => AMOUNT : " + packet.getAmount());
            IBCAPI.writeMessage(" => WRONG TOKEN : " + IBCAPI.get().isWrongToken());
            message = "";

            BluetoothManager manager = IBCAPI.get().getBluetoothManager();
            manager.connect(packet);
            Thread.sleep(200);
            String resp;
            while (!manager.send(resp = manager.encryptMessage(packet)))
                Thread.sleep(200);
            Log.d(IBCAPI.TAG, "Answer sent : " + resp);

            manager.close();
            VibratorUtil.vibrate(this.currentContext, VibrationEffect.createOneShot(2000, VibrationEffect.DEFAULT_AMPLITUDE));
            ToneGenerator generator = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
            generator.startTone(ToneGenerator.TONE_CDMA_PIP, 150);
            SystemClock.sleep(5000);
            generator.release();
        } catch (IncorrectPacketFormatException | IOException | InterruptedException ignored) {
        } finally {
            queried = false;
        }
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        if (enabled) {
            IBCAPI.get().getEventManager().registerListener(this);
        } else {
            IBCAPI.get().getEventManager().unregisterListener(this);
        }
        this.enabled = enabled;
    }
}
