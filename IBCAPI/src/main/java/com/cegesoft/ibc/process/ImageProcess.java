package com.cegesoft.ibc.process;

import com.cegesoft.ibc.IBCAPI;
import com.cegesoft.ibc.event.EventHandler;
import com.cegesoft.ibc.event.Listener;
import com.cegesoft.ibc.event.type.MessageReadEvent;
import com.cegesoft.ibc.event.type.PixelReceiveEvent;
import com.cegesoft.ibc.exceptions.IncorrectPacketFormatException;
import com.cegesoft.ibc.process.image.PixelPacket;

public class ImageProcess extends Process implements Listener {


    @Override
    protected void execute() {
        IBCAPI.get().getEventManager().registerListener(this);
        //this.runManually();
    }

    @Override
    protected void interruptProcess() {
        IBCAPI.get().getEventManager().unregisterListener(this);
    }

    @EventHandler
    public void onReceiveMessage(MessageReadEvent event) {
        try {
            PixelPacket packet = PixelPacket.fromString(event.getMessage());
            IBCAPI.get().getEventManager().callEvent(new PixelReceiveEvent(packet));
        } catch (IncorrectPacketFormatException e) {
            e.printStackTrace();
        }
    }
}
