package com.cegesoft.ibc.process;

public class ProcessHandler {

    private static Process process;

    public static void startProcess(Process process) {
        stopProcess();
        ProcessHandler.process = process;
        process.start();
    }

    public static void stopProcess() {
        if (ProcessHandler.process != null && !process.isInterrupted()) {
            ProcessHandler.process.interrupt();
        }
    }

    public static Process getProcess() {
        return process;
    }
}
