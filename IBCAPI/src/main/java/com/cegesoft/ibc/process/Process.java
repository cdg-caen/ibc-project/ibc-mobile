package com.cegesoft.ibc.process;

import android.util.Log;

import com.cegesoft.ibc.IBCAPI;

public abstract class Process extends Thread {

    public Process() {
        super("PROCESS-THREAD");
    }

    protected abstract void execute();
    protected abstract void interruptProcess();

    @Override
    public void run() {
        this.execute();
        super.run();
    }

    @Override
    public void interrupt() {
        this.interruptProcess();
        super.interrupt();
    }
}
