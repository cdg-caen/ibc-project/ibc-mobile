package com.cegesoft.ibc.process.image;

public class Dimension2D {

    private final double width, height;

    public Dimension2D(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public double getHeight() {
        return height;
    }
}
