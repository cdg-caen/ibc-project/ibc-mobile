package com.cegesoft.ibc.process.image;

import android.os.SystemClock;

import com.cegesoft.ibc.exceptions.IncorrectPacketFormatException;

public class PixelPacket {

    private final Pixel pixel;
    private final Location location;
    private final Dimension2D dimension2D;

    private PixelPacket(Pixel pixel, Location location, Dimension2D dimension2D) {
        this.pixel = pixel;
        this.location = location;
        this.dimension2D = dimension2D;
    }

    public Pixel getPixel() {
        return pixel;
    }

    public Location getLocation() {
        return location;
    }

    public Dimension2D getDimension2D() {
        return dimension2D;
    }

    public static PixelPacket fromString(String string) throws IncorrectPacketFormatException {
        string = string.split("#")[0];
        if (!string.contains(";"))
            throw new IncorrectPacketFormatException(string);
        String[] parts = string.split(";");
        if (parts.length != 2) {
            throw new IncorrectPacketFormatException(string);
        }
        try {
            int location = -Integer.parseInt(parts[0]);
            int width = (byte) (0xFF & (location));
            int x = (byte) (0xFF & (location >> 8));
            int y = (byte) (0xFF & (location >> 8 * 2));
            int height = width + (byte) (0xFF & (location >> 8 * 3));
//            int width = parts[0].equals("") ? 0 : Integer.parseInt(parts[0]);
//            int height = parts[1].equals("") ? width : width + Integer.parseInt(parts[1]);
//            int x = parts[2].equals("") ? 0 : Integer.parseInt(parts[2]);
//            int y = parts[3].equals("") ? 0 : Integer.parseInt(parts[3]);
            Pixel pixel = Pixel.fromString(parts[1].equals("") ? "0" : parts[1]);
            return new PixelPacket(pixel, new Location(x, y), new Dimension2D(width, height));
        } catch (Exception e) {
            throw new IncorrectPacketFormatException(string, e);
        }
    }
}
