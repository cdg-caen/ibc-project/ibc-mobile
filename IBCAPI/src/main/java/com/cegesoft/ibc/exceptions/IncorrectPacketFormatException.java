package com.cegesoft.ibc.exceptions;

public class IncorrectPacketFormatException extends Exception {

    public IncorrectPacketFormatException(String packet){
        super("Packet incorrectly formatted : " + packet);
    }

    public IncorrectPacketFormatException(String packet, String message){
        super(message + " " + packet);
    }

    public IncorrectPacketFormatException(String packet, Throwable thr){
        super("Packet incorrectly formatted : " + packet, thr);
    }

}
