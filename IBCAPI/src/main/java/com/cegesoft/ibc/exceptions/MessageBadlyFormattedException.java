package com.cegesoft.ibc.exceptions;

public class MessageBadlyFormattedException extends RuntimeException {

    public MessageBadlyFormattedException(String message, Throwable throwable){
        super("Can't read message '" + message + "'", throwable);
    }
    public MessageBadlyFormattedException(String message){
        super("Can't read message '" + message + "'");
    }
}
