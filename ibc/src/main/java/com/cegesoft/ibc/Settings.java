package com.cegesoft.ibc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import com.cegesoft.ibc.config.ConfigManager;
import com.cegesoft.ibc.sockets.Message;
import com.cegesoft.ibc.sockets.SocketConnection;
import com.cegesoft.ibc.user.User;

public class Settings extends AppCompatActivity {

    private EditText nameText, ipConfig, portConfig, baud, waitLevel;
    private SwitchCompat wrongToken;

    private String nameCache;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        IBCAPI.get().setCurrentActivity(this);

        baud = findViewById(R.id.baud);
        waitLevel = findViewById(R.id.waitLevel);

        nameText = findViewById(R.id.name_Text);
        ipConfig = findViewById(R.id.ip_config_text);
        portConfig = findViewById(R.id.port_config_text);

        wrongToken = findViewById(R.id.wrongToken);

        ConfigManager config = IBCAPI.get().getConfig();

        if (config.getIP() != null && !config.getIP().equals(""))
            ipConfig.setText(config.getIP());
        if (config.getPort() != 0)
            portConfig.setText(String.format("%s", config.getPort()));

        if (config.getBaud() != 0)
            baud.setText(String.format("%s", config.getBaud()));

        waitLevel.setText(String.format("%s", config.getMessageLimit()));

        wrongToken.setChecked(IBCAPI.get().isWrongToken());


        if ((config.getIP() != null && !config.getIP().equals("")) && config.getPort() != 0){
            nameText.setEnabled(false);
            new AsyncTask<Void, Void, Message>(){
                @Override
                protected Message doInBackground(Void... voids) {
                    try (SocketConnection socketConnection = new SocketConnection()) {
                        socketConnection.sendMessage(new Message(Message.Type.GET, Message.Request.NAME, User.CURRENT_USER), true);
                        return new Message(socketConnection.waitForAnswer());
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                protected void onPostExecute(Message message) {
                    if (message != null) {
                        nameText.setEnabled(true);
                        nameText.setText(nameCache = message.getArgs()[0]);
                    }
                }
            }.execute((Void) null);
        }
    }


    @SuppressLint("StaticFieldLeak")
    public void onSave(View view){
        final String name = nameText.getText().toString();
        String ip = ipConfig.getText().toString();
        String port = portConfig.getText().toString();
        String baud = this.baud.getText().toString();
        String limit = this.waitLevel.getText().toString();

        IBCAPI.get().getConfig().setIP(ip);
        IBCAPI.get().getConfig().setPort(Integer.parseInt(port));
        IBCAPI.get().getConfig().setBaud(Integer.parseInt(baud));
        IBCAPI.get().getConfig().setMessageLimit(Integer.parseInt(limit));
        IBCAPI.get().setWrongToken(wrongToken.isChecked());


        new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                if (nameCache != null && !nameCache.equals(name)){
                    try (SocketConnection socketConnection = new SocketConnection()){
                        socketConnection.sendMessage(new Message(Message.Type.POST, Message.Request.NAME, User.CURRENT_USER, name), true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void nul) {
                Settings.this.finish();
            }
        }.execute((Void) null);
    }

    public void onReset(View view) {
        IBCAPI.get().getConfig().reset();
        Settings.this.startActivity(new Intent(Settings.this, Register.class));
    }

    public void onClose(View view) {
        Settings.this.finish();
    }
}
