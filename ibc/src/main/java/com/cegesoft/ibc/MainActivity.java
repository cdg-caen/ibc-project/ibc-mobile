package com.cegesoft.ibc;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Scroller;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cegesoft.ibc.event.EventHandler;
import com.cegesoft.ibc.event.Listener;
import com.cegesoft.ibc.event.type.DeviceDisconnectedEvent;
import com.cegesoft.ibc.event.type.DeviceRecoverEvent;
import com.cegesoft.ibc.event.type.SerialReadEvent;
import com.cegesoft.ibc.event.type.WriteOnScreenEvent;
import com.cegesoft.ibc.process.MainProcess;
import com.cegesoft.ibc.process.ProcessHandler;

public class MainActivity extends AppCompatActivity implements Listener {

    public TextView state;
    public TextView LOGS;
    private boolean isServiceLaunched;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IBCAPI.get().setCurrentActivity(this);
        state = findViewById(R.id.state_view);
        LOGS = findViewById(R.id.logs);
        LOGS.setMovementMethod(new ScrollingMovementMethod());
        Log.d(IBCAPI.TAG, "Starting services");
        //        refreshDeviceList();
//        readMessage();
        if (!isServiceLaunched) {
            IBCAPI.get().getEventManager().registerListener(this);
            isServiceLaunched = true;
            ProcessHandler.startProcess(new MainProcess(this));
        }
    }

    @EventHandler
    public void onRead(SerialReadEvent event) {
        runOnUiThread(() -> {
            info("\n" + event.getRead());
        });
    }

    @EventHandler
    public void onMessage(WriteOnScreenEvent event) {
        runOnUiThread(() -> {
            info("\n" + event.getMessage());
        });
    }

    @EventHandler
    public void onConnect(DeviceRecoverEvent event) {
        Log.d(IBCAPI.TAG, "Device recovered !");
        runOnUiThread(() -> {
            state.setBackgroundColor(Color.parseColor("#FF2E7D32"));
            state.setText(R.string.state_ready);
        });
    }

    @EventHandler
    public void onDisconnect(DeviceDisconnectedEvent event) {
        Log.d(IBCAPI.TAG, "Device Disconnected !");
        runOnUiThread(() -> {
            state.setBackgroundColor(Color.parseColor("#C62828"));
            state.setText(R.string.state_wait);
        });
    }

    private void info(String message) {
        LOGS.append(message);
        if (this.LOGS.getScrollY() >= Math.max(this.LOGS.getLayout().getLineTop(this.LOGS.getLineCount()) - this.LOGS.getHeight(), 0) - 15)
            this.LOGS.scrollTo(0, Math.max(this.LOGS.getLayout().getLineTop(this.LOGS.getLineCount()) - this.LOGS.getHeight(), 0));
    }

    public void onSettingClick(View view) {
        final Intent intent = new Intent(this, Settings.class);
        this.startActivityForResult(intent, 1);
    }

    public void onShopClick(View view) {
        final Intent intent = new Intent(this, ShopActivity.class);
        this.startActivityForResult(intent, 1);
    }

}
