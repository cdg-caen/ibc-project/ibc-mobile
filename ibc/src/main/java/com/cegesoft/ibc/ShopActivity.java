package com.cegesoft.ibc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cegesoft.ibc.packets.IBCServerPacket;
import com.cegesoft.ibc.sockets.Message;
import com.cegesoft.ibc.sockets.SocketConnection;
import com.cegesoft.ibc.user.User;

import java.io.IOException;

public class ShopActivity extends AppCompatActivity {

    private Button backButton;
    private Button buyButton;
    private Button resetButton;
    private TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        this.result = this.findViewById(R.id.ticketView);
        this.buyButton = this.findViewById(R.id.buyButton);
        this.resetButton = this.findViewById(R.id.shopResetButton);
        this.backButton = this.findViewById(R.id.back);
        this.backButton.setOnClickListener(event -> {
            this.finish();
        });

        this.buyButton.setOnClickListener(event -> {
            setTicketTask("+", "1");
        });

        this.resetButton.setOnClickListener(event -> {
            setTicketTask("=", "0");
        });

        this.updateTickets();

    }

    private void updateTickets() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                Message message = new Message(Message.Type.GET, Message.Request.TICKETS, User.CURRENT_USER);

                try (SocketConnection connection = new SocketConnection()) {
                    connection.sendMessage(message, true);
                    int i = 0;
                    while (i < 4) {
                        try {
                            return connection.waitForAnswer();
                        } catch (Exception e) {
                            e.printStackTrace();
                            i++;
                            Thread.sleep(500);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return "Une erreur est survenue. Vérifiez votre connection.";
            }

            @Override
            protected void onPostExecute(String s) {
                if (s.startsWith("Une erreur")) {
                    result.setText(s);
                    return;
                }
                Message message = new Message(s);
                updateTicketsView(message.getArgs()[0]);
            }
        }.execute();
    }

    private void updateTicketsView(String tickets) {
        result.setText("Vous avez :\n" + tickets + " Tickets");
    }

    private void setTicketTask(String op, String value) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                Message message = new Message(Message.Type.POST, Message.Request.TICKETS, User.CURRENT_USER, op, value);

                try (SocketConnection connection = new SocketConnection()) {
                    connection.sendMessage(message, true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void s) {
                updateTickets();
            }
        }.execute();
    }
}