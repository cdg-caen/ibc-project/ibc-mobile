package com.cegesoft.ibc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cegesoft.ibc.config.ConfigManager;
import com.cegesoft.ibc.crypto.RSAKeyGenerator;
import com.cegesoft.ibc.process.MainProcess;
import com.cegesoft.ibc.process.ProcessHandler;
import com.cegesoft.ibc.service.ReadService;
import com.cegesoft.ibc.service.RecoveryService;
import com.cegesoft.ibc.sockets.Message;
import com.cegesoft.ibc.sockets.SocketConnection;
import com.cegesoft.ibc.user.User;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Random;


public class Register extends AppCompatActivity {

    private static boolean INIT = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        IBCAPI.get().setCurrentActivity(this);
/*        try {
            RSAKeyGenerator generator = new RSAKeyGenerator();

            String message = "Bonsoir je suis Clément";
            Log.d("Crypt", message);
            String encrypt = CryptoHandler.encrypt(message, generator.getPublicKeyString());
            Log.d("Crypt", encrypt);
            Log.d("Crypt", "PrivateKey : " + generator.getPrivateKeyString());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
*/
        bindService(new Intent(this, RecoveryService.class), RecoveryService.recoveryConnection, BIND_AUTO_CREATE);
        bindService(new Intent(this, ReadService.class), ReadService.readConnection, BIND_AUTO_CREATE);
        if (!IBCAPI.get().isInit()) {
            IBCAPI.get().init(this);
            IBCAPI.get().getConfig().setIP("10.0.0.2");
            User.CURRENT_USER = IBCAPI.get().getConfig().getUser();
            IBCAPI.get().setUsbManager((UsbManager)getSystemService(Context.USB_SERVICE));
            INIT = true;
            if (User.CURRENT_USER != null) {
                this.startActivity(new Intent(this, MainActivity.class));
                return;
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public void onStartPressed(View view) {
        EditText name = findViewById(R.id.registerNameText);
        final String text = name.getText().toString();
        if (text.equals("")) {
            Toast.makeText(this, "You must fill you name", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            final RSAKeyGenerator receiveGenerator = new RSAKeyGenerator();
            final RSAKeyGenerator sendGenerator = new RSAKeyGenerator();
            User.CURRENT_USER = new User(Integer.toString(new Random().nextInt(30000)), Integer.toString(new Random().nextInt(30000)), sendGenerator.getPublicKeyString(), receiveGenerator.getPrivateKeyString());
            IBCAPI.get().getConfig().saveUser();
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... voids) {
                    try {
                        Log.d(IBCAPI.TAG, new Message(Message.Type.POST, Message.Request.DECLARE, User.CURRENT_USER, User.CURRENT_USER.declare(text, sendGenerator.getPrivateKeyString(), receiveGenerator.getPublicKeyString())).parse(false));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try (SocketConnection connection = new SocketConnection()) {
                        connection.sendMessage(new Message(Message.Type.POST, Message.Request.DECLARE, User.CURRENT_USER, User.CURRENT_USER.declare(text, sendGenerator.getPrivateKeyString(), receiveGenerator.getPublicKeyString())), false);
                        return connection.waitForAnswer();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return "SUCCESS";
                }

                @Override
                protected void onPostExecute(String s) {
                    if (s.equalsIgnoreCase("SUCCESS"))
                        Register.this.startActivity(new Intent(Register.this, MainActivity.class));
                    else
                        Toast.makeText(Register.this, "An error occurred while updating server.", Toast.LENGTH_LONG).show();
                }
            }.execute();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return;
        }
    }

    @Override
    public void onBackPressed() {

    }
}
